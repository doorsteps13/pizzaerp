#!/bin/bash -e
echo "flake8 check: "
flake8 --exclude=*venv/ .
echo "pytest check: "
pytest
echo "mypy test: "
mypy ./
